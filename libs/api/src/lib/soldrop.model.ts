export interface SolDrop {
    id: number;
    project: string;
    launch: string;
    amount: number;
    mint: number;
    link1: string;
    link2: string;
    link3: string;
  }
  
  // export interface SolState {
  //   [id: string]: SolDrop;
  // }