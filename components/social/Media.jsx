import * as React from "react";
      

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGlobeEurope} from '@fortawesome/free-solid-svg-icons'
import { faTwitter, faDiscord } from '@fortawesome/free-brands-svg-icons'

export default function Media() {
  return (
    <React.Fragment>
      <Link href="/"><FontAwesomeIcon icon={faGlobeEurope} className="fa-2x"/></Link>&nbsp;
      <Link href="/"><FontAwesomeIcon icon={faTwitter}  className="fa-2x"/></Link>&nbsp;
      <Link href="/"><FontAwesomeIcon icon={faDiscord}  className="fa-2x"/></Link>
    </React.Fragment>
  );
}
