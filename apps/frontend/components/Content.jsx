import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

export default function Content() {
  return (
    <Box sx={{ width: '100%', marginTop: 4, marginLeft: 4, textAlign: 'left' }}>
      <Typography variant="h4" gutterBottom component="div">
        SOLdrops
      </Typography>
      <Typography variant="body1" gutterBottom>
          SOLdrops is your source for the hottest upcoming Solana NFT&#39;s project drops.
          <br />You&#39;ll find a compiled list of all upcoming NFT projects in one place.
          <br /><br />Projects listed below are not endorsed or verified by us.
          <br />Always DYOR before investing.
      </Typography>
    </Box>
  );
}