import React from "react";
import Image from 'next/image'
import { useForm } from "react-hook-form";

export default function WebForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => console.log(data);
  console.log(errors);

  return (
    <React.Fragment>
      <div style={{ marginLeft: 20}}>    
      <h3>Insert SOLDrop</h3>    
      <form onSubmit={handleSubmit(onSubmit)}>
      <p>Project</p>
      <input
        type="text"
        placeholder="Project"
        {...register("Project", { required: true, maxLength: 100 })}
      />
      <p>Launch</p>
      <input
        type="text"
        placeholder="Launch"
        {...register("Launch", { required: true, maxLength: 100 })}
      />
      <p>Amount</p>
      <input
        type="text"
        placeholder="Amount"
        size="5"
        maxLength="5"
        {...register("Amount", { required: true, maxLength: 5 })}
      />
      <p>Mint</p>
      <input
        type="text"
        placeholder="Mint"
        size="5"
        maxLength="5"
        {...register("Mint", { required: true, maxLength: 5 })}
      />
      <p>Facebook</p>
      <input
        type="text"
        placeholder="Facebook"
        {...register("Link1", { required: true, maxLength: 5 })}
      />
      <br />
      <Image src="images/links/facebook.png" alt="Facebook" style={{ marginTop: 20, width: 20, height: 20}} />
      <p>Twitter</p>
      <input
        type="text"
        placeholder="Twitter"
        {...register("Link2", { required: true, maxLength: 5 })}
      />
      <br />
      <Image src="images/links/twitter.png" alt="Twitter" style={{ marginTop: 20, width: 20, height: 20}} />
      <p>Discord</p>
      <input
        type="text"
        placeholder="Discord"
        {...register("Link3", { required: true, maxLength: 5 })}
      />
      <br />
      <Image src="images/links/discord.png" alt="Discord" style={{ marginTop: 20, width: 20, height: 20}} />
      <br />
      <br />
      <input type="submit" text=" Submit SOLDrop "/>
      </form>
    </div>
    </React.Fragment>
  );
}
