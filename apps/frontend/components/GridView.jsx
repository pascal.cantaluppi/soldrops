import * as React from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#2196f3",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

// const StyledTableRow = styled(TableRow)(({ theme }) => ({
//   "&:nth-of-type(odd)": {
//     backgroundColor: theme.palette.action.hover,
//   },
//   // hide last border
//   "&:last-child td, &:last-child th": {
//     border: 0,
//   },
// }));

// function createData(project, launch, amount, mint, link) {
//   return { project, launch, amount, mint, link};
// }

const renderTable = (data) => {
    return (
      <TableBody>
      {/* {data.map((row) => (
        <StyledTableRow key={row.id}>
          <StyledTableCell component="th" scope="row">
            {row.project}
          </StyledTableCell>
          <StyledTableCell align="right">{row.launch}</StyledTableCell>
          <StyledTableCell align="right">{row.amount}</StyledTableCell>
          <StyledTableCell align="right">{row.mint}</StyledTableCell>
          <StyledTableCell align="right">{row.link1}</StyledTableCell>
        </StyledTableRow>
      ))} */}
      </TableBody>
    )
  };


export default function GridView(props) {
  var json = props.json;
  //console.log("JSON: " + JSON.stringify(json));

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Project</StyledTableCell>
            <StyledTableCell align="right">Launch</StyledTableCell>
            <StyledTableCell align="right">Amount</StyledTableCell>
            <StyledTableCell align="right">Mint</StyledTableCell>
            <StyledTableCell align="right">Links</StyledTableCell>
          </TableRow>
        </TableHead>
        {renderTable(json.data)}
      </Table>
    </TableContainer>
  );
}
