import * as React from "react";
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGlobeEurope} from '@fortawesome/free-solid-svg-icons'
import { faTwitter, faDiscord } from '@fortawesome/free-brands-svg-icons'

export default function Media() {
  return (
    <React.Fragment>
      <Link href="/" passHref={true}><FontAwesomeIcon icon={faGlobeEurope} className="fa-2x"/></Link>&nbsp;
      <Link href="/" passHref={true}><FontAwesomeIcon icon={faTwitter} className="fa-2x"/></Link>&nbsp;
      <Link href="/" passHref={true}><FontAwesomeIcon icon={faDiscord} className="fa-2x"/></Link>
    </React.Fragment>
  );
}
