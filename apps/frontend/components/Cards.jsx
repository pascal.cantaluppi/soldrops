import * as React from "react";
import { experimentalStyled as styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import SolMediaCard from "./MediaCard";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export default function Cards() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}
      >
      <Grid item xs={2} sm={4} md={4} key="card1" alignItems="center" justify="center">
          <Item>
              <SolMediaCard alt="Solsteads" image="/images/cards/solsteads.png" title="Solsteads" content="Solsteads Solana&#39;s first surreal estate collection. Affordable housing NFT&#39;s. 10% of sale goes to charity. Sold out in under 30 seconds." />
            </Item>
          </Grid>
          <Grid item xs={2} sm={4} md={4} key="card2" alignItems="center" justify="center">
            <Item>
              <SolMediaCard alt="Galactic Gecko" image="/images/cards/galactic-gecko-space-garage.jpg" title="Galactic Gecko" content="Galactic Gecko Space Garage Solanas NFT Social Adventure Club - Join gecko heroes of the Interstellar Relay, a race that may decide the fate of the galaxy." />
            </Item>
          </Grid>
          <Grid item xs={2} sm={4} md={4} key="card3" alignItems="center" justify="center">
            <Item>
              <SolMediaCard alt="Infinity Labs" image="/images/cards/infinitylabs.png" title="Infinity Labs" content="Infinity Labs Collection of 8888 beautiful scenes. 8 different timelines. Collect & merge together timelines to forge new and rarer realities." />
            </Item>
          </Grid>
      </Grid>
    </Box>
  );
}
