import Head from "next/head";
import Image from 'next/image'
import Link from "next/link";      
import React from "react";
import Navigation from "../components/Navigation";
import GridView from "../components/GridView";
import Cards from "../components/Cards";
import Content from "../components/Content";
import Media from "../components/social/Media";
import axios from "axios";

export default function Home({sol = {}}) {
  return (
    <React.Fragment>
      <Head>
        <title>SOLDrops | NFTs on Solana</title>
        <meta name="description" content="SOLDrops | NFTs on Solana" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navigation />
      {/* style={{ backgroundColor: "#606060" }} */}
      <Content />
      <p>&nbsp;</p>
      <div style={{ marginLeft: 20, marginRight: 20}}>
        <GridView json={sol} />
      </div>
      <p>&nbsp;</p>
      {/* <ResponsiveGrid /> */}
      <Media />
      <br />
      <Link href="/" passHref={true}>
        <Image src="/images/coffee.svg" alt="Coffee" width={200} height={100} />
      </Link>
    </React.Fragment>
  );
}

Home.getInitialProps = async () => {
  try {
    const res = await axios.get(
      `/api/sol`
    );
    return { sol: res.data };
  } catch (error) {
    console.error(error);
    return {
      statusCode: error.response ? error.response.status : 500,
    };
  }
};
