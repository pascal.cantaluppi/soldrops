import React from "react";
import Head from "next/head";
import Error from "next/error";
//import SolAppBar from "../../components/SolAppBar";
import WebForm from "../../components/admin/WebForm";

const Admin = ({ statusCode }) => {
  if (statusCode) {
    return <Error statusCode={statusCode} />;
  }

  const renderAdmin = () => {
    // if (!statusCode) {
    // }
  };

  return (
    <React.Fragment>
      <Head>
        <title>SOLDrops | NFTs on Solana</title>
        <meta name="description" content="SOLDrops | NFTs on Solana" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {/* <SolAppBar style={{ backgroundColor: "#606060" }} /> */}
      {renderAdmin()}
      <WebForm />
    </React.Fragment>
  );
};

Admin.getInitialProps = async (context) => {
  //try {
    return null;
  //} catch (error) {
    // console.error(error);
    // return {
    //   statusCode: error.response ? error.response.status : 500,
    // };
  //}
};

export default Admin;
