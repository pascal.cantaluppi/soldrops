require('dotenv').config();
import axios from "axios";

async function handler(req, res) {
  var response = await axios.get(
    `${process.env.API }/drops`
  )
  res.status(200).json(response.data)
}

export default handler