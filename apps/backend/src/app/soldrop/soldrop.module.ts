import { Module } from '@nestjs/common';
import { SoldropController } from './soldrop.controller';
import { SoldropService } from './soldrop.service';

@Module({
  controllers: [SoldropController],
  providers: [SoldropService],
})
export class SoldropModule {}
