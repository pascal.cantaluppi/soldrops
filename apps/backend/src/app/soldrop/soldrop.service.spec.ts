import { Test, TestingModule } from '@nestjs/testing';
import { SoldropService } from './soldrop.service';

describe('SoldropService', () => {
  let service: SoldropService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SoldropService],
    }).compile();

    service = module.get<SoldropService>(SoldropService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
