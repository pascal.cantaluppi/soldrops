import { Controller, Get } from '@nestjs/common';
import { SoldropService } from './soldrop.service';
import { SolDrop } from "@soldrops/api"


@Controller('soldrop')
export class SoldropController {
  constructor(private soldropService: SoldropService) {}

  @Get()
  findAll(): SolDrop[] {
    console.log("psst!");
    return this.soldropService.getSoldrops();
  }
}