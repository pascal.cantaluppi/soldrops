import { Test, TestingModule } from '@nestjs/testing';
import { SoldropController } from './soldrop.controller';

describe('SoldropController', () => {
  let controller: SoldropController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SoldropController],
    }).compile();

    controller = module.get<SoldropController>(SoldropController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
