import { Injectable } from '@nestjs/common';

import { SolDrop } from "@soldrops/api"

const test: SolDrop[] = [
  {
    id: 1,
    project: 'Spirits of Solana 1',
    launch: '3 PM UTC',
    amount: 499,
    mint: 0.5,
    link1: 'index.html',
    link2: 'index.html',
    link3: 'index.html',
  },
  {
    id: 1,
    project: 'Spirits of Solana 2',
    launch: '3 PM UTC',
    amount: 499,
    mint: 0.5,
    link1: 'index.html',
    link2: 'index.html',
    link3: 'index.html',
  }
];

@Injectable()
export class SoldropService {

  getSoldrops(): SolDrop[] {
    return test;
  }
}