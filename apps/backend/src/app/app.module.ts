import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SoldropModule } from './soldrop/soldrop.module';

@Module({
  imports: [SoldropModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
